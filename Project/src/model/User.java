package model;

import java.io.Serializable;

/**
 * Userテーブルのデータを格納するためのBeans
 * @author takano
 *
 */

public class User implements Serializable {

	private int id;
	private String login_id;
	private String account_name;
	private String password;

	//アカウントを登録するコンストラクタ
	public User(String loginId, String accountname, String password) {
		this.login_id = loginId;
		this.account_name = accountname;
		this.password = password;

	}

	//ログインするコンストラクタ
	public User(String loginId, String password) {
		this.login_id = loginId;
		this.password = password;
	}


	public User(int id, String loginId, String accountname, String password) {
		this.id = id;
		this.login_id = loginId;
		this.account_name = accountname;
		this.password = password;
	}

	public User(String loginIdData) {
		this.login_id = loginIdData;
	}

	//account_idを取得
	public User(int idDate) {
		this.id=idDate;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getLogin_id() {
		return login_id;
	}

	public void setLogin_id(String login_id) {
		this.login_id = login_id;
	}

	public String getAccount_name() {
		return account_name;
	}

	public void setAccount_name(String account_name) {
		this.account_name = account_name;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
}