package model;

import java.io.Serializable;

public class Good_Bad implements Serializable {

	private String post_id;
	private String account_name;
	private int good;
	private int bad;



	public int getBad() {
		return bad;
	}



	public void setBad(int bad) {
		this.bad = bad;
	}



	public int getGood() {
		return good;
	}



	public void setGood(int good) {
		this.good = good;
	}



	public Good_Bad(String accountname) {
		this.account_name = accountname;
	}



	public String getPost_id() {
		return post_id;
	}

	public void setPost_id(String post_id) {
		this.post_id = post_id;
	}

	public String getAccount_name() {
		return account_name;
	}

	public void setAccount_name(String account_name) {
		this.account_name = account_name;
	}

}
