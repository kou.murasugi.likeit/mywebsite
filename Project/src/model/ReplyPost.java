package model;

import java.io.Serializable;

public class ReplyPost implements Serializable {

	private String create_date;
	private String post;
	private String id;
	private String account_name;

	//投稿時間を取得するコンストラクタ
	public ReplyPost(String createDate) {
		this.create_date = createDate;
	}

	//findAll
	public ReplyPost(String id, String post, String accountName, String createDate) {
		// TODO 自動生成されたコンストラクター・スタブ
		this.id = id;
		this.post = post;
		this.account_name = accountName;
		this.create_date = createDate;
	}

	public String getCreateDate() {
		return create_date;
	}

	public void setCreateDate(String create_date) {
		this.create_date = create_date;
	}

	public String getPost() {
		return post;
	}

	public void setPost(String post) {
		this.post = post;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getAccount_name() {
		return account_name;
	}

	public void setAccount_name(String account_name) {
		this.account_name = account_name;
	}
}
