package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import model.User;

public class UserDao {
	/**
	 * ログインIDとパスワードに紐づくユーザ情報を返す
	 * @param loginId
	 * @param password
	 * @return
	 */
	public static User findByLoginInfo(String loginId, String password) {
		Connection conn = null;
		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			String result = (password);
			System.out.println(result);

			// SELECT文を準備
			String sql = "SELECT * FROM account WHERE login_id = ? and password = ?";

			// SELECTを実行し、結果表を取得
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, loginId);
			pStmt.setString(2, password);
			ResultSet rs = pStmt.executeQuery();

			// 主キーに紐づくレコードは1件のみなので、rs.next()は1回だけ行う
			if (!rs.next()) {
				return null;
			}

			// 必要なデータのみインスタンスのフィールドに追加
			String loginIdData = rs.getString("login_id");
			String nameData = rs.getString("account_name");
			return new User(loginIdData, nameData);

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}

	public User findByRegisterInfo(String loginId) {
		Connection conn = null;
		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// SELECT文を準備
			String sql = "SELECT * FROM account WHERE login_id = ?";

			// SELECTを実行し、結果表を取得
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, loginId);
			ResultSet rs = pStmt.executeQuery();

			// 主キーに紐づくレコードは1件のみなので、rs.next()は1回だけ行う
			if (!rs.next()) {
				return null;
			}

			// 必要なデータのみインスタンスのフィールドに追加
			String loginIdData = rs.getString("login_id");
			return new User(loginIdData);

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}

	//アカウント情報を登録する処理

	public void insert(String loginId, String password, String accountname) {
		Connection con = null;
		PreparedStatement stmt = null;

		try {
			// データベース接続
			con = DBManager.getConnection();

			// 実行SQL文字列定義
			String insertSQL = "INSERT INTO account(login_id,password,account_name) VALUES(?,?,?)";
			// ステートメント生成
			stmt = con.prepareStatement(insertSQL);
			// SQLの?パラメータに値を設定
			stmt.setString(1, loginId);
			stmt.setString(2, password);
			stmt.setString(3, accountname);

			// 登録SQL実行
			stmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
			System.out.println("入力された内容は正しくありません");
		} finally {
			try {
				// ステートメントインスタンスがnullでない場合、クローズ処理を実行
				if (stmt != null) {
					stmt.close();
				}
				// コネクションインスタンスがnullでない場合、クローズ処理を実行
				if (con != null) {
					con.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}

		}
	}

	//アカウント情報を取得
	public User findByDetail(String login_id) {
		Connection conn = null;
		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// SELECT文を準備
			String sql = "SELECT * FROM account WHERE login_id = ?";
			//↑ここも変更する（先にSQLで正常に動作するか確認する)

			// SELECTを実行し、結果表を取得
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, login_id);//
			ResultSet rs = pStmt.executeQuery();

			// 主キーに紐づくレコードは1件のみなので、rs.next()は1回だけ行う
			if (!rs.next()) {
				return null;
			}

			// 必要なデータのみインスタンスのフィールドに追加
			int idDate = rs.getInt("id");
			String loginIdDate = rs.getString("login_id");
			String accountnameDate = rs.getString("account_name");
			String passwordDate = rs.getString("password");
			return new User(idDate, loginIdDate, accountnameDate, passwordDate);

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}

	//ユーザー更新
	public void UPDATE(String loginId, String accountname, String password, String id) {
		// TODO 自動生成されたメソッド・スタブ
		Connection con = null;
		PreparedStatement stmt = null;

		try {
			// データベース接続
			con = DBManager.getConnection();

			// 実行SQL文字列定義
			String insertSQL = "UPDATE account SET login_id=?,password=?,account_name=? WHERE id=?;";
			// ステートメント生成
			stmt = con.prepareStatement(insertSQL);
			// SQLの?パラメータに値を設定
			stmt.setString(1, loginId);
			stmt.setString(2, password);
			stmt.setString(3, accountname);
			stmt.setString(4, id);

			// 登録SQL実行
			stmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
			System.out.println("入力された内容は正しくありません");
		} finally {
			try {
				// ステートメントインスタンスがnullでない場合、クローズ処理を実行
				if (stmt != null) {
					stmt.close();
				}
				// コネクションインスタンスがnullでない場合、クローズ処理を実行
				if (con != null) {
					con.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}

		}
	}

	public User findByUpdate(String id) {
		Connection conn = null;
		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// SELECT文を準備
			String sql = "SELECT * FROM account WHERE id = ?";

			// SELECTを実行し、結果表を取得
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, id);
			ResultSet rs = pStmt.executeQuery();

			// 主キーに紐づくレコードは1件のみなので、rs.next()は1回だけ行う
			if (!rs.next()) {
				return null;
			}

			// 必要なデータのみインスタンスのフィールドに追加
			int idDate = rs.getInt("id");
			String loginIdDate = rs.getString("login_id");
			String passwordDate = rs.getString("password");
			String accountnameDate = rs.getString("account_name");
			return new User(idDate, loginIdDate, passwordDate, accountnameDate);
			//上記のコンストラクタ作成した　

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}

	//IDが重複しているか
	public static boolean isOverlapLoginId(String loginId) throws SQLException {
		// 重複しているかどうか表す変数
		boolean isOverlap = false;
		Connection con = null;
		PreparedStatement st = null;

		try {
			con = DBManager.getConnection();
			// 入力されたlogin_idが存在するか調べる
			st = con.prepareStatement("SELECT login_id FROM account WHERE login_id = ?");
			st.setString(1, loginId);
			ResultSet rs = st.executeQuery();

			System.out.println("searching loginId by inputLoginId has been completed");

			if (rs.next()) {
				isOverlap = true;
			}
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}

		System.out.println("overlap check has been completed");
		return isOverlap;
	}

	//アカウント削除
	public void DELETE(String Id) {
		// TODO 自動生成されたメソッド・スタブ
		Connection con = null;
		PreparedStatement stmt = null;

		try {
			// データベース接続
			con = DBManager.getConnection();
			// 実行SQL文字列定義
			String deleteSQL = "DELETE FROM account WHERE id=?;";
			// ステートメント生成
			stmt = con.prepareStatement(deleteSQL);
			// SQLの?パラメータに値を設定
			stmt.setString(1, Id);

			// 登録SQL実行
			stmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
			System.out.println("アカウントの削除に失敗しました。");
		} finally {
			try {
				// ステートメントインスタンスがnullでない場合、クローズ処理を実行
				if (stmt != null) {
					stmt.close();
				}
				// コネクションインスタンスがnullでない場合、クローズ処理を実行
				if (con != null) {
					con.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}

		}
	}

	//account_idを取得
	public int findByDetail2(String login_id) {
		Connection conn = null;
		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// SELECT文を準備
			String sql = "SELECT * FROM account WHERE login_id = ?";
			//↑ここも変更する（先にSQLで正常に動作するか確認する)

			// SELECTを実行し、結果表を取得
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, login_id);//
			ResultSet rs = pStmt.executeQuery();

			// 主キーに紐づくレコードは1件のみなので、rs.next()は1回だけ行う
			if (!rs.next()) {
				return (Integer) null;
			}
			// 必要なデータのみインスタンスのフィールドに追加
			int idDate = rs.getInt("id");
			return idDate;
		} catch (SQLException e) {
			e.printStackTrace();
			return (Integer) null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return (Integer) null;
				}
			}
		}
	}
}