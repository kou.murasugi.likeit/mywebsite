package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import model.Good_Bad;

public class Reply_Good_BadDao {

	//高評価を押した人の情報登録処理
		public Reply_Good_BadDao insert(int good, String post_id, String account_id) {
			Connection con = null;
			PreparedStatement stmt = null;

			try {
				// データベース接続
				con = DBManager.getConnection();

				// 実行SQL文字列定義
				String insertSQL = "INSERT INTO  reply_good_bad(good_bad,post_id,account_id) VALUES (?,?,?);";
				// ステートメント生成
				stmt = con.prepareStatement(insertSQL);
				// SQLの?パラメータに値を設定
				stmt.setInt(1, good);
				stmt.setString(2, post_id);
				stmt.setString(3, account_id);
				// 登録SQL実行
				stmt.executeUpdate();
			} catch (SQLException e) {
				e.printStackTrace();
				System.out.println("エラー発生");
			} finally {
				try {
					// ステートメントインスタンスがnullでない場合、クローズ処理を実行
					if (stmt != null) {
						stmt.close();
					}
					// コネクションインスタンスがnullでない場合、クローズ処理を実行
					if (con != null) {
						con.close();
					}
				} catch (SQLException e) {
					e.printStackTrace();
				}

			}
			return null;
		}

		//高評価を押した全てのアカウントを取得
		public List<Good_Bad> findAll1(String post_id) {
			Connection conn = null;
			List<Good_Bad> replygoodList = new ArrayList<Good_Bad>();

			try {
				// データベースへ接続
				conn = DBManager.getConnection();

				String sql = "SELECT * FROM  reply_good_bad"
						+ " t INNER JOIN account t1 ON t.account_id=t1.id "
						+ "WHERE t.post_id=? AND t.good_bad=1";
				//結合
				// SELECTを実行し、結果表を取得
				PreparedStatement pStmt = conn.prepareStatement(sql);
				pStmt.setString(1, post_id);
				ResultSet rs = pStmt.executeQuery();


				while (rs.next()) {
					String AccountName = rs.getString("account_name");
					Good_Bad good = new Good_Bad(AccountName);
					replygoodList.add(good);
				}
			} catch (SQLException e) {
				e.printStackTrace();
				return null;
			} finally {
				// データベース切断
				if (conn != null) {
					try {
						conn.close();
					} catch (SQLException e) {
						e.printStackTrace();
						return null;
					}
				}
			}
			return replygoodList;
		}

		//低評価を押した人の情報登録処理
		public Reply_Good_BadDao insert2(int bad, String post_id, String account_id) {
			Connection con = null;
			PreparedStatement stmt = null;

			try {
				// データベース接続
				con = DBManager.getConnection();

				// 実行SQL文字列定義
				String insertSQL = "INSERT INTO  reply_good_bad(good_bad,post_id,account_id) VALUES (?,?,?);";
				// ステートメント生成
				stmt = con.prepareStatement(insertSQL);
				// SQLの?パラメータに値を設定
				stmt.setInt(1, bad);
				stmt.setString(2, post_id);
				stmt.setString(3, account_id);
				// 登録SQL実行
				stmt.executeUpdate();
			} catch (SQLException e) {
				e.printStackTrace();
				System.out.println("エラー発生");
			} finally {
				try {
					// ステートメントインスタンスがnullでない場合、クローズ処理を実行
					if (stmt != null) {
						stmt.close();
					}
					// コネクションインスタンスがnullでない場合、クローズ処理を実行
					if (con != null) {
						con.close();
					}
				} catch (SQLException e) {
					e.printStackTrace();
				}

			}
			return null;
		}

		//低評価を押した全てのアカウントを取得
		public List<Good_Bad> findAll2(String post_id) {
			Connection conn = null;
			List<Good_Bad> replybadList = new ArrayList<Good_Bad>();

			try {
				// データベースへ接続
				conn = DBManager.getConnection();

				String sql = "SELECT * FROM  reply_good_bad"
						+ " t INNER JOIN account t1 ON t.account_id=t1.id "
						+ "WHERE t.post_id=? AND t.good_bad=2";
				//結合
				// SELECTを実行し、結果表を取得

				PreparedStatement pStmt = conn.prepareStatement(sql);
				pStmt.setString(1, post_id);
				ResultSet rs = pStmt.executeQuery();

				while (rs.next()) {
					String AccountName = rs.getString("account_name");
					Good_Bad bad = new Good_Bad(AccountName);
					replybadList.add(bad);
				}
			} catch (SQLException e) {
				e.printStackTrace();
				return null;
			} finally {
				// データベース切断
				if (conn != null) {
					try {
						conn.close();
					} catch (SQLException e) {
						e.printStackTrace();
						return null;
					}
				}
			}
			return replybadList;
		}

}
