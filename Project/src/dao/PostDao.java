package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import model.Post;

public class PostDao {

	//投稿する処理

	public Post insert(String post, String account_id) {
		Connection con = null;
		PreparedStatement stmt = null;

		try {
			// データベース接続
			con = DBManager.getConnection();

			// 実行SQL文字列定義
			String insertSQL = "INSERT INTO post(post,create_date,account_id) VALUES(?,NOW(),?);";
			// ステートメント生成
			stmt = con.prepareStatement(insertSQL);
			// SQLの?パラメータに値を設定
			stmt.setString(1, post);
			stmt.setString(2, account_id);
			// 登録SQL実行
			stmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
			System.out.println("投稿できませんでした");
		} finally {
			try {
				// ステートメントインスタンスがnullでない場合、クローズ処理を実行
				if (stmt != null) {
					stmt.close();
				}
				// コネクションインスタンスがnullでない場合、クローズ処理を実行
				if (con != null) {
					con.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}

		}
		return null;
	}

	//投稿時間を取得する処理

	public Post findByCreate_Date(String id) {
		Connection conn = null;
		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// SELECT文を準備
			String sql = "SELECT create_date FROM post WHERE id=?;";

			// SELECTを実行し、結果表を取得
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, id);
			ResultSet rs = pStmt.executeQuery();

			// 主キーに紐づくレコードは1件のみなので、rs.next()は1回だけ行う
			if (!rs.next()) {
				return null;
			}

			// 必要なデータのみインスタンスのフィールドに追加
			String Create_Date = rs.getString("create_date");
			return new Post(Create_Date);

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}

	//投稿削除
	public void DELETE(String Id) {
		// TODO 自動生成されたメソッド・スタブ
		Connection con = null;
		PreparedStatement stmt = null;

		try {
			// データベース接続
			con = DBManager.getConnection();
			// 実行SQL文字列定義
			String deleteSQL = "DELETE FROM post WHERE id=?;";
			// ステートメント生成
			stmt = con.prepareStatement(deleteSQL);
			// SQLの?パラメータに値を設定
			stmt.setString(1, Id);

			// 登録SQL実行
			stmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
			System.out.println("");
		} finally {
			try {
				// ステートメントインスタンスがnullでない場合、クローズ処理を実行
				if (stmt != null) {
					stmt.close();
				}
				// コネクションインスタンスがnullでない場合、クローズ処理を実行
				if (con != null) {
					con.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}

		}
	}

	/**
	 * 全ての投稿情報を取得する
	 * @return
	 */
	public List<Post> findAll() {
		Connection conn = null;
		List<Post> postList = new ArrayList<Post>();

		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			String sql = "SELECT * FROM post t INNER JOIN account t1 ON t.account_id=t1.id ORDER BY t.create_date DESC";
			//結合
			// SELECTを実行し、結果表を取得
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);

			// 結果表に格納されたレコードの内容を
			// Userインスタンスに設定し、ArrayListインスタンスに追加
			while (rs.next()) {
				String Id = rs.getString("id");
				String Post = rs.getString("post");
				String accountName = rs.getString("account_name");
				String createDate = rs.getString("create_date");
				Post post = new Post(Id, Post, accountName, createDate);

				postList.add(post);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return postList;
	}
}
