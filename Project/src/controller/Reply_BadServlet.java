package controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.Reply_Good_BadDao;
import model.Good_Bad;

/**
 * Servlet implementation class Reply_BadServlet
 */
@WebServlet("/Reply_BadServlet")
public class Reply_BadServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public Reply_BadServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		// リクエストパラメータの文字コードを指定
		request.setCharacterEncoding("UTF-8");

		HttpSession session = request.getSession();

		try {

			//ログイン中のaccount_id
			String Id = request.getParameter("account_id");

			int Bad = 2;

			//PostId
			String PostId = request.getParameter("replypostid");
			request.setAttribute("replypostid", PostId);

			//評価を登録 2=低評価
			Reply_Good_BadDao good_badDao = new Reply_Good_BadDao();
			good_badDao.insert(Bad, PostId, Id);

			//低評価リストを取得
			Reply_Good_BadDao good_badDao2 = new Reply_Good_BadDao();
			List<Good_Bad> replybadList = good_badDao2.findAll2(PostId);

			// リクエストスコープにユーザ一覧情報をセット
			request.setAttribute("replybadList", replybadList);

			//高評価リストを取得
			Reply_Good_BadDao good_Dao2 = new Reply_Good_BadDao();
			List<Good_Bad> replygoodList = good_Dao2.findAll1(PostId);

			// リクエストスコープにユーザ一覧情報をセット
			request.setAttribute("replygoodList", replygoodList);

			// reply_good_badlist jspにフォワード
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/reply_good_badlist.jsp");
			dispatcher.forward(request, response);
		} catch (Exception e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
			response.sendRedirect("ErrorServlet");
		}
	}
}
