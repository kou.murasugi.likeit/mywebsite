package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.ReplyPostDao;

/**
 * Servlet implementation class ReplyPostDeleteServlet
 */
@WebServlet("/ReplyPostDeleteServlet")
public class ReplyPostDeleteServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public ReplyPostDeleteServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");

		try {
			// 返信
			String ReplyPost = request.getParameter("id");

			request.setAttribute("ReplyPost", ReplyPost);

			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/replypostdelete.jsp");
			dispatcher.forward(request, response);
		} catch (Exception e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
			response.sendRedirect("ErrorServlet");
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		request.setCharacterEncoding("UTF-8");

		try {
			// URLからGETパラメータとしてidを受け取る
			String ReplyPost = request.getParameter("ReplyPost");

			//Daoインスタンス生成
			ReplyPostDao ReplypostDao = new ReplyPostDao();
			ReplypostDao.DELETE(ReplyPost);

			// ReplyListServlet リダイレクト
			response.sendRedirect("IndexServlet");
		} catch (Exception e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
			response.sendRedirect("ErrorServlet");
		}
	}
}
