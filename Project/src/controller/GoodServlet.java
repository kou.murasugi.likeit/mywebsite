package controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.Good_BadDao;
import model.Good_Bad;

/**
 * Servlet implementation class Post_Good_BadServlet
 */
@WebServlet("/GoodServlet")
public class GoodServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public GoodServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// リクエストパラメータの文字コードを指定
		request.setCharacterEncoding("UTF-8");

		HttpSession session = request.getSession();

		try {

			//ログイン中のaccount_id
			String Id = request.getParameter("account_id");

			int Good=1;

			//PostId
			String PostId = request.getParameter("postid");
			request.setAttribute("postid", PostId);

			//評価を登録 1=高評価
			Good_BadDao good_badDao = new Good_BadDao();
			good_badDao.insert(Good,PostId,Id);

			//高評価リストを取得
			Good_BadDao good_badDao2 = new Good_BadDao();
			List<Good_Bad> goodList = good_badDao2.findAll1(PostId);
            //account_name○,good,bad=0

			//低評価リストを取得
			Good_BadDao badDao = new Good_BadDao();
			List<Good_Bad> badList = badDao.findAll2(PostId);

			// リクエストスコープにユーザ一覧情報をセット
			request.setAttribute("badList", badList);

			// リクエストスコープにユーザ一覧情報をセット
			request.setAttribute("goodList", goodList);

			// goodbadlist jspにフォワード
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/goodbadlist.jsp");
			dispatcher.forward(request, response);

		} catch (Exception e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
			response.sendRedirect("ErrorServlet");
		}
	}
}
