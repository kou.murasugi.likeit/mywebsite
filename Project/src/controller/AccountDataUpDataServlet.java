package controller;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.UserDao;
import model.User;

/**
 * Servlet implementation class AccountDataUpDataSevlet
 */
@WebServlet("/AccountDataUpDataServlet")
public class AccountDataUpDataServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public AccountDataUpDataServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		// リクエストパラメータの文字コードを指定
		request.setCharacterEncoding("UTF-8");

		// セッション開始
		HttpSession session = request.getSession();

		try {
			User user = (User) session.getAttribute("userInfo");

			//Daoインスタンス生成
			UserDao userDao = new UserDao();
			User user1 = userDao.findByDetail(user.getLogin_id());

			request.setAttribute("user", user1);

			// accountdataupdata.jsp フォワード
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/accountdataupdata.jsp");
			dispatcher.forward(request, response);
		} catch (Exception e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
			response.sendRedirect("ErrorServlet");
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		/*文字化け対策*/
		request.setCharacterEncoding("UTF-8");

		// セッション開始
		HttpSession session = request.getSession();

			User user = (User) session.getAttribute("userInfo");

			// リクエストパラメータの入力項目を取得
			String Id = request.getParameter("Id");
			String loginId = request.getParameter("loginId");//jspのinputのnameの部分
			String Password = request.getParameter("password");
			String Password2 = request.getParameter("password2");
			String AccountName = request.getParameter("accountname");

			//未入力時処理
			if (loginId.equals("") || Password.equals("") || Password2.equals("") || AccountName.equals("")) {
				// error jspにフォワード
				RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/error.jsp");
				dispatcher.forward(request, response);
				return;
			}

			//登録失敗時 パスワードの内容が異なる
			if (!(Password.equals(Password2))) {
				// error jspにフォワード
				RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/error.jsp");
				dispatcher.forward(request, response);
				return;
			}

			//loginIdの重複をチェック
			try {
				if (UserDao.isOverlapLoginId(loginId)) {
				}
			} catch (SQLException e) {
				// TODO 自動生成された catch ブロック
				e.printStackTrace();
				// error jspにフォワード
				RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/error.jsp");
				dispatcher.forward(request, response);
			}

			//Daoインスタンス生成
			UserDao userDao2 = new UserDao();
			userDao2.UPDATE(loginId, AccountName, Password, Id);

			response.sendRedirect("AccountDataServlet");
	}
}
