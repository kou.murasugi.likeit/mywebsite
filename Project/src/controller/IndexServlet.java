package controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.PostDao;
import dao.UserDao;
import model.Post;
import model.User;

/**
 * Servlet implementation class IndexServlet
 */
@WebServlet("/IndexServlet")
public class IndexServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public IndexServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		HttpSession session = request.getSession();

		try {
			User user = (User) session.getAttribute("userInfo");

			if (user == null) {
				// ログインのサーブレットにリダイレクト
				response.sendRedirect("LoginServlet");
				return;
			}

			// 投稿一覧情報を取得
			PostDao postDao = new PostDao();
			List<Post> postList = postDao.findAll();
            //account_name=1

			// リクエストスコープにユーザ一覧情報をセット
			request.setAttribute("postList", postList);

			//Daoインスタンス生成
			UserDao userDao = new UserDao();
			User user1 = userDao.findByDetail(user.getLogin_id());

			session.setAttribute("user", user1);

			//account_id 取得
			UserDao userDao2 = new UserDao();
			int account_id = userDao2.findByDetail2(user.getLogin_id());

			request.setAttribute("account_id", account_id);

			// index.jsp フォワード
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/index.jsp");
			dispatcher.forward(request, response);
		} catch (Exception e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
			response.sendRedirect("ErrorServlet");
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		HttpSession session = request.getSession();

		try {
			//投稿情報セッションを取得
			String Id = (String) session.getAttribute("Id");
			String Post = (String) session.getAttribute("Post");
			String AccountName = (String) session.getAttribute("AccountName");
			String CreateDate = (String) session.getAttribute("CreateDate");

			//投稿情報をセッションにセット
			session.setAttribute("Id", Id);
			session.setAttribute("Post", Post);
			session.setAttribute("AccountName", AccountName);
			session.setAttribute("CreateDate", CreateDate);

			// index.jsp フォワード
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/index.jsp");
			dispatcher.forward(request, response);
		} catch (Exception e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
			response.sendRedirect("ErrorServlet");
		}
	}
}