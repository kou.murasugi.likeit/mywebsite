package controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.Reply_Good_BadDao;
import model.Good_Bad;

/**
 * Servlet implementation class Reply_Good_BadListServlet
 */
@WebServlet("/Reply_Good_BadListServlet")
public class Reply_Good_BadListServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public Reply_Good_BadListServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		try {
			//PostId(返信）
			String PostId = request.getParameter("replypostid");
			request.setAttribute("replypostid", PostId);

			//高評価リストを取得
			Reply_Good_BadDao good_Dao2 = new Reply_Good_BadDao();
			List<Good_Bad> replygoodList = good_Dao2.findAll1(PostId);

			// リクエストスコープにユーザ一覧情報をセット
			request.setAttribute("replygoodList", replygoodList);


			//低評価リストを取得
			Reply_Good_BadDao badDao2 = new Reply_Good_BadDao();
			List<Good_Bad> replybadList = badDao2.findAll2(PostId);

			// リクエストスコープにユーザ一覧情報をセット
			request.setAttribute("replybadList", replybadList);

			// reply_good_badlist.jsp フォワード
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/reply_good_badlist.jsp");
			dispatcher.forward(request, response);
		} catch (Exception e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
			response.sendRedirect("ErrorServlet");
		}
	}
}
