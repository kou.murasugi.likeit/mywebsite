package controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.ReplyPostDao;
import dao.UserDao;
import model.ReplyPost;
import model.User;

/**
 * Servlet implementation class ReplyListServlet
 */
@WebServlet("/ReplyListServlet")
public class ReplyListServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public ReplyListServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		// セッション開始
		HttpSession session = request.getSession();

		try {
			User user = (User) session.getAttribute("userInfo");
			//login_id

			// 投稿,URLからGETパラメータとしてidを受け取る
			String PostId = request.getParameter("postid");

			// 投稿(返信)一覧情報を取得
			ReplyPostDao replypostDao = new ReplyPostDao();
			List<ReplyPost> replypostList = replypostDao.findReplylist(PostId);

			// リクエストスコープにユーザ一覧情報をセット
			request.setAttribute("replypostList", replypostList);

			//Daoインスタンス生成
			UserDao userDao = new UserDao();
			User user1 = userDao.findByDetail(user.getLogin_id());

			session.setAttribute("user", user1);

			//account_id 取得
			UserDao userDao2 = new UserDao();
			int account_id = userDao2.findByDetail2(user.getLogin_id());

			request.setAttribute("account_id", account_id);

			//replylist.jsp フォワード
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/replylist.jsp");
			dispatcher.forward(request, response);

		} catch (Exception e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
			response.sendRedirect("ErrorServlet");
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

	}

}
