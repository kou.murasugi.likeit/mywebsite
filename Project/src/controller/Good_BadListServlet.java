package controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.Good_BadDao;
import model.Good_Bad;

/**
 * Servlet implementation class Good_BadListServlet
 */
@WebServlet("/Good_BadListServlet")
public class Good_BadListServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public Good_BadListServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		try {
			//PostId
			String PostId = request.getParameter("postid");
			request.setAttribute("postid", PostId);

			//高評価リストを取得
			Good_BadDao good_Dao = new Good_BadDao();
			List<Good_Bad> goodList = good_Dao.findAll1(PostId);
			//account_name○,good,bad=0

			// リクエストスコープにユーザ一覧情報をセット
			request.setAttribute("goodList", goodList);

			//低評価リストを取得
			Good_BadDao badDao = new Good_BadDao();
			List<Good_Bad> badList = badDao.findAll2(PostId);

			// リクエストスコープにユーザ一覧情報をセット
			request.setAttribute("badList", badList);

			// goodbadlist.jsp フォワード
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/goodbadlist.jsp");
			dispatcher.forward(request, response);
		} catch (Exception e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
			response.sendRedirect("ErrorServlet");
		}
	}
}