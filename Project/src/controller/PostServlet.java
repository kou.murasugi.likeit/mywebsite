package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.PostDao;
import dao.UserDao;
import model.User;

/**
 * Servlet implementation class PostServlet
 */
@WebServlet("/PostServlet")
public class PostServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public PostServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		// リクエストパラメータの文字コードを指定
		request.setCharacterEncoding("UTF-8");

		// セッション開始
		HttpSession session = request.getSession();

		try {
			User user = (User) session.getAttribute("userInfo");

			//Daoインスタンス生成
			UserDao userDao = new UserDao();
			User user1 = userDao.findByDetail(user.getLogin_id());

			request.setAttribute("user", user1);
			//accountname,id,login_id,password取得済み

			// post.jspにフォワード
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/post.jsp");
			dispatcher.forward(request, response);
			return;
		} catch (Exception e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
			response.sendRedirect("ErrorServlet");
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		request.setCharacterEncoding("UTF-8");

		// セッション開始
		HttpSession session = request.getSession();

		try {
			User user = (User) session.getAttribute("userInfo");

			// リクエストパラメータの入力項目を取得
			String Id = request.getParameter("Id");
			String Post = request.getParameter("Post");
			String AccountName = request.getParameter("accountname");
			//Id=accountid

			// リクエストパラメータの入力項目を引数に渡して、Daoのメソッドを実行
			PostDao postDao = new PostDao();
			postDao.insert(Post, Id);
			//実行できている

			//IndexServlet リダイレクト
			response.sendRedirect("IndexServlet");
		} catch (Exception e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
			response.sendRedirect("ErrorServlet");
		}
	}
}
