<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>

<head>
<meta charset="UTF-8">
<title>アカウント情報更新確認</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">
<link href="style.css" rel="stylesheet" type="text/css" />
</head>

<body>
	<!-- ヘッダー -->
	<header>
		<h1>3 ちゃんねる</h1>
	</header>
	<ul class="nav justify-content-end">
		<li class="nav-item"><a class="nav-link active"
			href="PostServlet">投稿する</a></li>
		<li class="nav-item"><a class="nav-link" href="IndexServlet">タイムライン</a></li>
		<li class="nav-item"><a class="nav-link"
			href="AccountDataServlet">アカウント情報</a></li>
		<c:if test="${userInfo.login_id==null}">
			<li class="nav-item"><a class="nav-link" href="RegistServlet">新規登録</a></li>
		</c:if>
		<li class="nav-item"><a class="nav-link disabled"
			href="LogoutServlet" tabindex="-1" aria-disabled="true">ログアウト</a></li>
	</ul>
	<!-- ここまで -->
	<div class="col sm-2">
		<div class="container">
			<p></p>
			<p>アカウント情報更新確認</p>
			<form action="IndexServlet" method="post">
				<div class="form-group row">
					<input type="hidden" name="Id" value="${user.id}"> <label
						for="staticEmail" class="col-sm-2 col-form-label">ログインID</label>
					<div class="col-sm-10">
						<input type="text" readonly class="form-control-plaintext"
							name="loginId" id="staticEmail" value="${user.login_id}">
					</div>
					<label for="staticEmail" class="col-sm-2 col-form-label">パスワード</label>
					<div class="col-sm-10">
						<input type="text" readonly class="form-control-plaintext"
							name="password" id="staticEmail" value="${user.password}">
					</div>
					<label for="staticEmail" class="col-sm-2 col-form-label">パスワード（確認）</label>
					<div class="col-sm-10">
						<input type="text" readonly class="form-control-plaintext"
							name="password2" id="staticEmail" value="${user.password}">
					</div>
					<label for="staticEmail" class="col-sm-2 col-form-label">アカウント名</label>
					<div class="col-sm-10">
						<input type="text" readonly class="form-control-plaintext"
							name="accountname" id="staticEmail" value="${user.account_name}">
					</div>
					<div class="mt-3">
						<button type="submit" style="width: 95px;" class="btn btn-primary">完了</button>
						<a type="button" style="width: 100px;" class="btn btn-danger"
							href="AccountDataUpDataServlet">キャンセル</a>
					</div>
				</div>
			</form>
		</div>
	</div>
</body>

</html>
