<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>３ちゃんねるへようこそ</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">
<link href="style.css" rel="stylesheet" type="text/css" />
</head>

<body>
	<!-- ヘッダー -->
	<header>
		<h1>　3 ちゃんねる</h1>
	</header>
	<p></p>
	<c:if test="${errMsg != null}">
		<div class="alert alert-danger" role="alert">${errMsg}</div>
	</c:if>
	<ul class="nav justify-content-end">
		<li class="nav-item"><a class="nav-link" href="RegistServlet">新規登録</a></li>
	</ul>
	<!-- ここまで -->
	<div class="col sm-2">
		<div class="container">
			<div class="mt-5">
				<p>アカウントをお持ちの方はログインしてください</p>
				<form class="form-signin" action="LoginServlet" method="post">
					<div class="form-group">
						<label for="LoginID">ログインID</label> <input type="LoginID"
							style="width: 200px;" class="form-control" id="Login ID"
							name="loginId"
                    aria-describedby="
							emailHelp" placeholder="ログインID">
					</div>
					<div class="form-group">
						<label for="exampleInputPassword1">パスワード</label> <input
							type="password" style="width: 200px;" class="form-control"
							id="exampleInputPassword1" name="password" placeholder="パスワード">
					</div>
					<div class="mt-3">
						<button type="submit" style="width: 95px;" class="btn btn-primary">ログイン</button>
						<button type="submit" style="width: 100px;" class="btn btn-danger">キャンセル</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</body>
</html>