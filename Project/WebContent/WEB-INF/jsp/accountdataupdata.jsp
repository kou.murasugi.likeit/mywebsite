<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>アカウント情報更新</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">
<link href="style.css" rel="stylesheet" type="text/css" />
</head>
<body>
	<!-- ヘッダー -->
	<header>
		<h1>3 ちゃんねる</h1>
	</header>
	<ul class="nav justify-content-end">
		<li class="nav-item"><a class="nav-link active"
			href="PostServlet">投稿する</a></li>
		<li class="nav-item"><a class="nav-link" href="IndexServlet">タイムライン</a></li>
		<li class="nav-item"><a class="nav-link"
			href="AccountDataServlet">アカウント情報</a></li>
		<c:if test="${userInfo.login_id==null}">
			<li class="nav-item"><a class="nav-link" href="RegistServlet">新規登録</a></li>
		</c:if>
		<li class="nav-item"><a class="nav-link disabled"
			href="LogoutServlet" tabindex="-1" aria-disabled="true">ログアウト</a></li>
	</ul>
	<!-- ここまで -->
	<div class="col sm-2">
		<div class="container">
			<p></p>
			<p>アカウント情報更新</p>
			<form action="AccountDataUpDataServlet" method="post">
				<c:if test="${validationMessage != null}">
					<p class="red-text center-align">${validationMessage}</p>
				</c:if>
				<div class="form-group">
					<input type="hidden" name="Id" value="${user.id}"> <label
						for="LoginID">ログインID</label> <input style="width: 200px;"
						class="form-control" id="Login ID" value="${user.login_id}"
						aria-describedby="emailHelp" name="loginId" placeholder="ログインID">
				</div>
				<div class="form-group">
					<label for="exampleInputPassword1">パスワード</label> <input
						type="password" style="width: 200px;" class="form-control"
						id="exampleInputPassword1" name="password"
						value="${user.password}">
				</div>
				<div class="form-group">
					<label for="exampleInputPassword1">パスワード（確認）</label> <input
						type="password" style="width: 200px;" class="form-control"
						id="exampleInputPassword1" name="password2"
						value="${user.password}">
				</div>
				<div class="form-group">
					<label for="exampleInputPassword1">アカウント名</label> <input
						style="width: 200px;" class="form-control"
						id="exampleInputPassword1" name="accountname"
						value="${user.account_name}">
				</div>
				<button type="submit" style="width: 95px;" class="btn btn-primary">確認</button>
				<a type="submit" style="width: 100px;" class="btn btn-danger"
					href="IndexServlet">キャンセル</a>
				<p></p>
				<a type="submit" style="width: 200px;" class="btn btn-danger"
					href="AccountDeleteServlet?id=${user.id}">アカウント削除</a>
			</form>
		</div>
	</div>
</body>
</html>