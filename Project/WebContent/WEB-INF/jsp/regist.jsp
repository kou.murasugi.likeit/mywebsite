<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>アカウント登録</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link href="style.css" rel="stylesheet" type="text/css" />
</head>

<body>
    <header>
        <h1>　3 ちゃんねる</h1>
    </header>
    <div class="col sm-2">
        <div class="container">
            <p> </p>
            <p>新規登録してアカウントを作成してください</p>
            <form action="RegistServlet" method="post">
                <div class="form-group">
                    <label for="LoginID">ログインID</label>
                    <input type="LoginID" style="width:200px;" class="form-control" id="Login ID" name="loginId"aria-describedby="emailHelp" placeholder="ログインID">
                </div>
                <div class="form-group">
                    <label for="exampleInputPassword1">パスワード</label>
                    <input type="password" style="width:200px;" class="form-control" id="exampleInputPassword1"  name="password" placeholder="パスワード">
                </div>
                <div class="form-group">
                    <label for="exampleInputPassword1">パスワード（確認）</label>
                    <input type="password" style="width:200px;" class="form-control" id="exampleInputPassword1" name="password2" placeholder="パスワード（確認）">
                </div>
                <div class="form-group">
                    <label for="exampleInputPassword1">アカウント名</label>
                    <input type="text" style="width:200px;" class="form-control" id="accountname"  name="accountname" placeholder="アカウント名">
                </div>
                <button type="submit" style="width:95px;" class="btn btn-primary">確認</button>
                <button type="submit" style="width:100px;" class="btn btn-danger" >キャンセル</button>
            </form>
        </div>
    </div>
</body>

</html>
