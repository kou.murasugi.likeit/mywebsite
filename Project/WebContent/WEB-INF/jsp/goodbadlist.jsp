<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>

<head>
<meta charset="UTF-8">
<title></title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">
<link href="style.css" rel="stylesheet" type="text/css" />
</head>

<body>
	<!-- ヘッダー -->
	<header>
		<h1>3 ちゃんねる</h1>
	</header>
	<ul class="nav justify-content-end">
		<li class="nav-item"><a class="nav-link active"
			href="PostServlet">投稿する</a></li>
		<li class="nav-item"><a class="nav-link" href="IndexServlet">タイムライン</a></li>
		<li class="nav-item"><a class="nav-link"
			href="AccountDataServlet">アカウント情報</a></li>
		<c:if test="${userInfo.login_id==null}">
			<li class="nav-item"><a class="nav-link" href="RegistServlet">新規登録</a></li>
		</c:if>
		<li class="nav-item"><a class="nav-link disabled"
			href="LogoutServlet" tabindex="-1" aria-disabled="true">ログアウト</a></li>
	</ul>
	<!-- ここまで -->
	<form>
		<div class="col sm-2">
			<div class="container">
				<h3>
					👍を押した人
					<c:forEach var="goodlist" items="${goodList}">
						<input type="text" readonly class="form-control-plaintext"
							value="${goodlist.account_name}">
					</c:forEach>
				</h3>
				<div class="mt-3">
					<h3>
						👎を押した人
						<c:forEach var="badlist" items="${badList}">
							<input type="text" readonly class="form-control-plaintext"
								value="${badlist.account_name}">
						</c:forEach>
					</h3>
				</div>
			</div>
		</div>
	</form>
</body>

</html>
