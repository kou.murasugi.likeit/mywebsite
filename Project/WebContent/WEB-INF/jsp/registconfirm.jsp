<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>

<head>
<meta charset="UTF-8">
<title>アカウント登録確認</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">
</head>

<body>
	<header>
		<h1>3 ちゃんねる</h1>
	</header>
	<div class="col sm-2">
		<div class="container">
			<p></p>
			<p>アカウント登録確認</p>
			<form action="RegistConfirmServlet" method="post">
				<div class="form-group row">
					<label for="staticEmail" class="col-sm-2 col-form-label">ログインID</label>
					<div class="col-sm-10">
						<input type="text" readonly class="form-control-plaintext"
							id="staticEmail" name="loginId" value="${loginId}">
					</div>
					<label for="staticEmail" class="col-sm-2 col-form-label">パスワード</label>
					<div class="col-sm-10">
						<input type="text" readonly class="form-control-plaintext"
							id="staticEmail" name="password" value="${password}">
					</div>
					<label for="staticEmail" class="col-sm-2 col-form-label">アカウント名</label>
					<div class="col-sm-10">
						<input type="text" readonly class="form-control-plaintext"
							id="staticEmail" name="accountname" value="${accountname}">
					</div>
				</div>
				<div class="mt-3">
					<button type="submit" style="width: 95px;" class="btn btn-primary">完了</button>
					<a href="RegistServlet" style="width: 100px;"
						class="btn btn-danger">キャンセル</a>
				</div>
			</form>
		</div>
	</div>
</body>

</html>
