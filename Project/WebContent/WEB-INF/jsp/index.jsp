<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>

<head>
<meta charset="UTF-8">
<title></title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">
<link href="style.css" rel="stylesheet" type="text/css" />
</head>

<body>
	<!-- ヘッダー -->
	<header>
		<h1>　3 ちゃんねる</h1>
	</header>
	<ul class="nav justify-content-end">
		<li class="nav-item"><a class="nav-link active"
			href="PostServlet">投稿する</a></li>
		<li class="nav-item"><a class="nav-link" href="IndexServlet">タイムライン</a>
		</li>
		<li class="nav-item"><a class="nav-link"
			href="AccountDataServlet">アカウント情報</a></li>
			<c:if test="${userInfo.login_id==null}">
		<li class="nav-item">
		<a class="nav-link" href="RegistServlet">新規登録</a>
		</li>
		</c:if>
		<li class="nav-item"><a class="nav-link disabled"
			href="LogoutServlet" tabindex="-1" aria-disabled="true">ログアウト</a></li>
	</ul>
	<!-- ヘッダー -->
	<!-- 投稿 -->
	<form>
	<c:forEach var="post" items="${postList}">
		<input  type="hidden" value="${post.id}" name="postid">
		<div class="col sm-2">
			<div class="container">
				<div class="form-group">
					<input type="text" readonly class="form-control-plaintext" value="${post.account_name}">
					<textarea readonly class="form-control" id="exampleFormControlTextarea1" rows="3">
                  ${post.post}
                    </textarea>
					<input type="text" readonly class="form-control-plaintext"
						value="${post.createDate}">
						 <a type="button"
						class="btn btn-outline-dark" href="ReplyServlet?postid=${post.id}">💭</a>
						<a type="submit" class="btn btn-outline-dark" href="ReplyListServlet?postid=${post.id}">💭リスト</a>
				     	<a type="submit" class="btn btn-outline-dark" href="GoodServlet?postid=${post.id}&account_id=${account_id}">👍</a>
						<a type="submit" class="btn btn-outline-dark" href="BadServlet?postid=${post.id}&account_id=${account_id}">👎</a>
						<a type="submit" class="btn btn-outline-dark" href="Good_BadListServlet?postid=${post.id}">👍👎リスト</a>
						<a type="submit" class="btn btn-outline-dark" href="PostDeleteServlet?id=${post.id}">✖</a>
					<h5>＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿</h5>
				</div>
			</div>
		</div>
		</c:forEach>
	</form>
</body>

</html>
